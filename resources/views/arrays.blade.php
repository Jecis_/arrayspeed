<!DOCTYPE html>
<html>
<head>
    <!-- Site made with Mobirise Website Builder v4.9.3, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v4.9.3, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo2-1.png" type="image/x-icon">
    <meta name="description" content="">
    <title>Home</title>
    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/dropdown/css/style.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">

    <style>
        td {
            word-break: break-all;

        }
.apraksts{
    text-align: left!important;
}
        h4 {
            margin-top: 50px;
        }
    </style>
</head>
<body>
<section class="menu cid-riNol1SBkp" once="menu" id="menu1-0">


    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">

                <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-7" href="/">Kārtošanas ātrumi</a></span>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">


        </div>
    </nav>
</section>

<section class="header5 cid-riNoMPPAFd mbr-fullscreen" id="header5-1">


    <div class="container">
        <div class="row justify-content-center">
            <div class="mbr-white col-md-10">
                <h1 class="mbr-section-title align-center pb-3 mbr-fonts-style display-1">Masīvu kārtošana ar dažādām
                    metodēm - ātruma tests</h1>
                <p class="mbr-text align-center display-5 pb-3 mbr-fonts-style">Uzlabota apmaiņas metode (šeikeris) ,
                    Horra(QuickSort),Kvadrātsaknes,
                    Šella metode , BubleSort(pāra un nepāra elementu apmaiņas metode), &nbsp;Secīgas apvienošanas kopā
                    šķirošana,Skirosana ar saskaitisanu</p>
            </div>
        </div>
    </div>

    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>

<section class="step1 cid-riNqHOdf14" id="step1-5" style="background-color: #72b81c">


    <div class="container">
        <h2 class="mbr-section-title pb-3 mbr-fonts-style align-center display-2">
            Kā mēs aprēķinām kārtošanas ātrumu</h2>
        <h3 class="mbr-section-subtitle pb-5 mbr-fonts-style align-center display-5"></h3>
        <div class="step-container">
            <div class="card separline pb-4">
                <div class="step-element d-flex">
                    <div class="step-wrapper pr-3">
                        <h3 class="step d-flex align-items-center justify-content-center">
                            1
                        </h3>
                    </div>
                    <div class="step-text-content">
                        <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">Mēs izmantojam mūsu metodes, lai
                            sakārtotu trīs tipu masīvus </h4>
                        <p class="mbr-step-text mbr-fonts-style display-7">Pirmais masīva tips ir random skaitļi robežās
                            no -100 &nbsp;līdz 100.
                            <br>Otrais masīvs ir ar skaitļiem no 1 līdz Masīva izmēram.<br>Trešais masīvs ir ar
                            skaitļiem no masīva izmēra size līdz 0.
                        </p>
                    </div>
                </div>
            </div>

            <div class="card separline pb-4">
                <div class="step-element d-flex">
                    <div class="step-wrapper pr-3">
                        <h3 class="step d-flex align-items-center justify-content-center">
                            2
                        </h3>
                    </div>
                    <div class="step-text-content">
                        <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">Mēs palaižam šķirošanas algoritmus
                            daudzas reizes </h4>
                        <p class="mbr-step-text mbr-fonts-style display-7">Mēs palaižam šos kārtošanas algoritmus
                            daudzas reizes dodot tiem kārtot dažāda izmēra masīvus gan uz PHP gan java,
                            PHP rezultātus uzreiz saglabājot datubāzē apstrādei,taču Java resultātus saglabājot csv un
                            importējot datubāzē. &nbsp;</p>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="step-element d-flex">
                    <div class="step-wrapper pr-3">
                        <h3 class="step d-flex align-items-center justify-content-center">
                            3
                        </h3>
                    </div>
                    <div class="step-text-content">
                        <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">Aprēķinam vidējo izpildes laiku </h4>
                        <p class="mbr-step-text mbr-fonts-style display-7">
                            Katrai metodei mēs aprēķinām vidējo izpildes laiku. Sākumā izmantojot vienu milzīgu masīvu
                            ar 10000 elementiem,
                            tad masīvu ar no 100 līdz 10000 elementiem (no 100 līdz 1000 solis nākamajam masīva izmēram
                            ir 100, taču no 1000 līdz 10000 solis ir 1000),
                            visbeidzot masīvu ar maziem skaitļiem no 10 līdz 100 ar nākamā masīva izmēra soli 10.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="progress-bars1 cid-riNqOpHdON" id="progress-bars1-6"
         style=" padding-right: 10%; padding-left: 10% ;background-color: #72b81c ; max-width: 100%!important;">
    <h2>Masīvi ģenerēti php </h2>
    <h3>Laiks norādīts sekundēs</h3>
    <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">
        Izpildes laiki robežās no 10000 - 10000 </h4>
    <div class="table-responsive">
        <table class="table table-striped table-dark" style="max-width: 100%!important;">
            <thead>
            <tr>
                <th scope="col">Masīva veids</th>

                @foreach($firstbound as $key=> $value)
                    <th scope="col">{{$value['method']}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>Random no -100 līdz 100</td>

                @foreach($firstbound as  $value)
                    <td>{{$value['times'][0]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 0 - 10000</td>

                @foreach($firstbound as  $value)
                    <td>{{$value['times'][1]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 10000 - 0</td>
                @foreach($firstbound as  $value)
                    <td>{{$value['times'][2]}}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
    </div>

    <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">
        Izpildes laiki robežās no 100 - 10000</h4>
    <div class="table-responsive">
        <table class="table table-striped table-dark" style="max-width: 100%!important;">
            <thead>
            <tr>
                <th scope="col">Masīva veids</th>

                @foreach($secondbound as $key=> $value)
                    <th scope="col">{{$value['method']}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>Random no -100 - 100</td>

                @foreach($secondbound as  $value)
                    <td>{{$value['times'][0]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 0 - 10000</td>

                @foreach($secondbound as  $value)
                    <td>{{$value['times'][1]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 10000 - 0</td>
                @foreach($secondbound as  $value)
                    <td>{{$value['times'][2]}}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
    </div>
    <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">
        Izpildes laiki robežās no 10 - 100
    </h4>
    <div class="table-responsive">
        <table class="table table-striped table-dark" style="max-width: 100%!important;">
            <thead>
            <tr>
                <th scope="col">Masīva veids</th>

                @foreach($thirdbound as $key=> $value)
                    <th scope="col">{{$value['method']}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>Random no -100 - 100</td>

                @foreach($thirdbound as  $value)
                    <td>{{$value['times'][0]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 10 - 100</td>

                @foreach($thirdbound as  $value)
                    <td>{{$value['times'][1]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 100 - 10</td>
                @foreach($thirdbound as  $value)
                    <td>{{$value['times'][2]}}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
    </div>
</section>
<section class="progress-bars1 cid-riNqOpHdON" id="progress-bars1-6"
         style=" padding-right: 10%; padding-left: 10% ;background-color: #72b81c ; max-width: 100%!important;">
    <h2>Masīvi ģenerēti Java </h2>
    <h3>Laiks norādīts nanosekundēs</h3>

    <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">
        Izpildes laiki robežās no 10000 - 10000 </h4>
    <div class="table-responsive">
        <table class="table table-striped table-dark" style="max-width: 100%!important;">
            <thead>
            <tr>
                <th scope="col">Masīva veids</th>

                @foreach($firstbound1 as $key=> $value)
                    <th scope="col">{{$value['method']}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>Random no -100 līdz 100</td>

                @foreach($firstbound1 as  $value)
                    <td>{{$value['times'][0]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 0 - 10000</td>

                @foreach($firstbound1 as  $value)
                    <td>{{$value['times'][1]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 10000 - 0</td>
                @foreach($firstbound1 as  $value)
                    <td>{{$value['times'][2]}}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
    </div>
    <div id="firstresult" style="width: 100%;height:400px;"></div>
    <h3>*Lai apskatītu precīzāku rezultātu uzejiet uz grafika ar peli un paritiniet vidējo pogu vai izmantojiet pelēko
        līniju</h3>
    <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">
        Izpildes laiki robežās no 100 - 10000</h4>
    <div class="table-responsive">
        <table class="table table-striped table-dark" style="max-width: 100%!important;">
            <thead>
            <tr>
                <th scope="col">Masīva veids</th>

                @foreach($secondbound2 as $key=> $value)
                    <th scope="col">{{$value['method']}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>Random no -100 - 100</td>

                @foreach($secondbound2 as  $value)
                    <td>{{$value['times'][0]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 0 - 10000</td>

                @foreach($secondbound2 as  $value)
                    <td>{{$value['times'][1]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 10000 - 0</td>
                @foreach($secondbound2 as  $value)
                    <td>{{$value['times'][2]}}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
    </div>
    <div id="secondresult" style="width: 100%;height:400px;"></div>
    <h3>*Lai apskatītu precīzāku rezultātu uzejiet uz grafika ar peli un paritiniet vidējo pogu vai izmantojiet pelēko
        līniju</h3>

    <h4 class="mbr-step-title pb-3 mbr-fonts-style display-5">
        Izpildes laiki robežās no 10 - 100
    </h4>
    <div class="table-responsive">
        <table class="table table-striped table-dark" style="max-width: 100%!important;">
            <thead>
            <tr>
                <th scope="col">Masīva veids</th>

                @foreach($thirdbound3 as $key=> $value)
                    <th scope="col">{{$value['method']}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>

            <tr>
                <td>Random no -100 - 100</td>

                @foreach($thirdbound3 as  $value)
                    <td>{{$value['times'][0]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 10 - 100</td>

                @foreach($thirdbound3 as  $value)
                    <td>{{$value['times'][1]}}</td>
                @endforeach
            </tr>
            <tr>
                <td>secīgi skaitļi no 100 - 10</td>
                @foreach($thirdbound3 as  $value)
                    <td>{{$value['times'][2]}}</td>
                @endforeach
            </tr>
            </tbody>
        </table>
    </div>
    <div id="thirdresult" style="width: 100%;height:400px;"></div>
    <h3>*Lai apskatītu precīzāku rezultātu uzejiet uz grafika ar peli un paritiniet vidējo pogu vai izmantojiet pelēko
        līniju</h3>

</section>
<section class="progress-bars1 cid-riNqOpHdON" id="progress-bars1-6"
         style=" padding-right: 10%; padding-left: 10% ;background-color: #72b81c ; max-width: 100%!important;">
    <h1>Metožu apraksti </h1>
    <h2>Uzlabota apmaiņas metode (šeikeris) </h2>
    <h3 class="apraksts">
        Metode balstās uz tiešas apmaiņas metodes. No tiešas apmaiņas metodes atšķiras ar to, ka elementu salīdzināšana
        tiek
        izpildīta pretējos virzienos: no masīva sākuma līdz beigām un atpakaļ.Atkārtot, kamēr masīvs netiks sakārtots
        (kamēr izpildās elementu apmaiņas operācijas).
    </h3>
    <h2>Horara(QuickSort) </h2>
    <h3>
        Metodes ideja ir sekojoša:1.Atrast mediānu. Par masīva mediānu sauc to masīva elementu, kas ir mazāks vai
        vienāds ar masīva elementavienu pusi
        un lielāks vai vienāds par masīva elementa otru
        pusi. 2. Sadalīt masīvu divās daļās un masīva sākumā izvietot elementus mazākus nekā mediāna, bet masīva beigās
        elementus, kas ir lielāki
        nekā mediāna.3. Atkārtot algoritmu katrai daļai, kamēr daļas izmērs nekļūst vienāds ar vienu elementu.Mediānas
        meklēšanai parasti izmanto
        sekojošus paņēmienus:•nejauši izvēlaselementuun izvēlētāelementa vērtību izmanto kā mediānas
        vērtību; nejaušiizvēlas 3 elementus un elementu ar vidējo vērtībuizmanto
        kā mediānas vērtību(parasti ņem pirmo, pēdējo un elementu, kas atrodas masīva vidū); izvēlas 1% no elementiem un
        meklēvidējo, kuru izmanto kā mediānas vērtību. Lai masīva
        sākumā izvietotuelementus mazākus nekā mediāna, bet masīva beigās elementus, kas ir lielāki nekā mediāna parasti
        izmanto sekojošu algoritmu: 1. Šķirošanas procesa sākumā i=0,
        j=n-1. 2. Palielināt i par vienu, kamēr netiks atrasts elements, kas ir lielāks nekā mediāna.
        163.Samazināt j par vienu, kamēr netiks atrasts elements, kas ir mazāks nekā mediāna. 4. Apmainīt vietām elementus
        ar indeksiem i un j. 5. Atkārtot punktus 2-4 kamēr i !=j .
    </h3>
    <h2>Kvadrātsaknes</h2>
    <h3>
        1. Masīvs, kuram ir N elementu tiek sadalīts N grupās, katrā no kurām ir N elementu.
        Ja sadalīt veselos skaitļos nav iespējams, tiek izveidota papildgrupa, kurā ir jāizvieto visi pārējie elementi.
        2. Katrā grupā tiek sameklēti vismazākie elementi, kurus izvieto speciālos buferos. Šajā gadījumā, šos elementus
        pamatmasīvā iznīcina.
        3. Katrā buferī tiek pārskatīti elementi ar vismazāko vērtību un vismazākā vērtība tiek ierakstīta izejas
        masīvā, bet atbilstošs buferis tiek iztukšots.
        Dotā bufera numurs m tiek saglabāts atmiņā. 4. Grupā ar numuru m atkal sameklē vismazāko elementu, kurš tiek
        ierakstīts atbilstošā buferī. Ja grupā elementu vairs nav,
        tad buferis paliks tukšs.5.
        Tiek pārbaudīts vai tukši ir visi buferi: ja nē, tad atgriežas pie 3. punkta izpildes, ja jā, tad izejas masīvs
        satur sakārtotu elementu secību.
    </h3>
    <h2>Šella metode </h2>
    <h3>
        Balstās uz tiešās iekļaušanas metodes. Tiešas iekļaujas metode darbojas lēni, jo tiešas iekļaujas metodē tiek
        pārvietoti tikai blakus elementi. Tas noved pie tā, ka elementi pārvietojas tikai uz vienu soli.Šella metodes
        ātrdarbība tiek sasniegta tāpēc, ka tiek lietota iespēja apmainīt vietām elementus, kas atrodas tālu viens no
        otra.Sākumā ar tiešas iekļaujas metodi tiek sakārtoti tikai tie elementi, kas atrodas viens no otra
        attālumāh.Pēc tam attālums h pakāpeniski tiek samazināts, kamēr nekļūst vienāds ar 1 (masīvs tiek
        sakārtots).Piemēram, ja attālums h ir vienāds ar 4, tad sākuma ar tiešās iekļaujas metoditiek sakārtoti
        elementi, kas ir iekrāsoti ar zilo krāsu, pēc tam elementi, kas ir iekrāsoti ar zaļo krāsu,pēc tam ar dzelteno
        krāsu un pēdējā kārtā elementi ar balto krāsu. Pēc tam attālums tiek samazināts un viss atkārtojas no jauna,
        kamēr attālums netiks vienāds ar 1 un masīvs netiks sakārtots.
        Ir svarīgi pareizi izvēlēties attālumu secību, jo laba attālumu secība var paaugstināt algoritma ātrumu par 25%.
        Ir zināms, ka attālumi nedrīkst atšķirties ar patstāvīgu reizinātāju. Piemēram attālumu secība, kuru piedāvāja
        1959. gadā metodes autors, ir slikta (2, 4, 8, 16, ...).Labas secības piemērs ir sekojošs (dotosecību piedāvāja
        D.Knuth 1969. gadā):1 4 13 40 121 364 1093... Katru nākošoattālumu dotā secība var aprēķināt izmantojot formulu:
        hk = 3hk-1+ 1, bet kopējo algoritma soļu skaitu (attālumu secību) var noteikt pēc formulas t = [log3n]
        –1.Piezīme: Aizvietot logaritma bāzi var izmantojot formulu: Vēl vienu labo secību var aprēķinātizmantojot
        formulas: hk = 2hk-1+ 1 un t = [log2n] –1.Tādējādi, Šella metodes algoritmsir sekojošs:1. Izveidot attālumu
        secību h[t] (var izmantot masīvu)2. Pārcilāt visus attālumus un katram attālumam izpildīt tiešas iekļaujas
        metodi, lai sakārtotu visus elementus, kas atrodas viens no otra norādītā attālumā(ir jāsakārto visas elementu
        apakškopas, kuru indeksi atšķiras ar izvēlēto attālumu).
    </h3>
    <h2>BubleSort(pāra un nepāra elementu apmaiņas metode)</h2>
    <h3>
        Metode balstās uz tiešas apmaiņas metodes.Pirmā solī salīdzināt un apmainīt vietām masīva elementus ar numuriem
        i un i+1, kur i ir pāra skaitlis.Otrā solī salīdzināt un apmainīt vietām masīva elementus ar numuriem i un i+1,
        kur i ir nepāra skaitlis.Atkārtot kamēr masīvs netiks sakārtots (kamēr izpildās elementu apmaiņas operācijas).
    </h3>
    <h2> Secīgas apvienošanas kopā šķirošana</h2>
    <h3>
        Ir apmaiņas sakārtošanas modifikācija. Ja tiek sakārtots masīvs A, kuram ir
        n elementu, tad ir nepieciešams masīvs B kurš ir arī no nelementiem.No sākuma papildu masīvā B izvieto masīva A
        sakārtotus elementu pārus.Pēc tam papild masīva B sakārtotus pārus apvieno kopāun, tos sakārtojot,ieraksta
        masīvā A.Rezultātāmasīvā Atiks izvietotas četru sakārtotu elementu secības. Pārejot no masīva A masīvā B un
        atpakaļ katru reizi dubultojas sakārtotu secību garums. Kad sakārtotu secību garums kļūsvienāds ar dotā masīva
        garumu n, šķirošanas procesu var pārtraukt. Šajā gadījumā, ja sakārtotu elementu secība atrodas masīvā B, tad to
        ir jāpārsūta masīvā A.Divu sakārtotu secību apvienošanas algoritms var būt sekojošs, pieņemsim ir divi sakārtoti
        masīvi x un y, kurus ir jāapvieno sakārtotā masīvā z, tad:1. Piešķirt i=0, j=0 un k=0.2. Salīdzināt x[i] ar y[j]
        un mazāko elementu ierakstītelementā z[k]. Palielināt par vieninieku k vērtību. Ja masīvā z tika ierakstīts
        elements no masīva x, tad palielināt arī i vērtību. Ja masīvā z tika ierakstīts elementsno masīva y, tad
        palielināt j vērtību. 3. Ja masīvā x elementu vairs nav, tad ierakstīt masīva z palikušos elementus no masīva
        y. 4. Ja masīvā y elementu vairs nav, tad ierakstīt masīva z palikušos elementus no masīva x.
    </h3>
    <h2>Skirosana ar saskaitisanu </h2>
    <h3>
        Metodes ideja balstās uz to, ka sakārtotā secībā katra elementa vērtība ir lielāka pat to elementu vērtībām, kas
        ir izvietotiiepriekš. Piemēram, ja kāds sakārtota masīva elements ir lielāks par 27 masīva elementiem, tad tas
        aizņem 28 vietu. Tādējādi, katra atsevišķa elementa vērtība ir jāsalīdzina ar visu pārējo masīva elementu
        vērtībām un jāsaskaita, cik no tiem ir mazāki par katra atsevišķa elementa vērtībām. Ja masīvā ir vienādi
        elementi, tieir jāizvieto viensotram blakām. Dotā metode parasti tiek realizēta ar papildus masīviem, kas glabā
        informāciju par to, cik daudz elementu ir mazāki, nekā tekošais elements un cik daudz elementu ir vienādi ar
        tekošo elementu. Algoritma izpildes laiku var aprakstīt kā O(N2), jo, katrs elements ir jāsalīdzina ar katru
        elementu.Metodes izmantošana ir agrūtināta, kad masīvs satur papildus informāciju.
        Piezīme:Eksistē dotā algoritma modifikācija, kuraizpildes laiku var aprakstīt kā O(N) (tas ir labāk nekā Hoara
        metodes rezultāti), bet doto modifikāciju var izmantot tikai masīviem no veseliem skaitļiem, ja elementu vērtību
        diapazons nav liels (jo algoritms pieprasa daudz papildus atminas).Algoritma idejair sekojoša. Ja masīva
        elementi pieņem vērtības no m līdz n, tad izveidot papildus masīvuno n-m+1 elementiem un katram papildus masīva
        elementam ar numuru i piešķirt elementu skaitu vienādu ar i+m.Lai to izdarītu ir nepieciešams pārcilāt visus
        sākotnējāmasīva elementus,palielinot par vieninieku atbilstošus papildus masīva elementus. Pēc tam, izmantojot
        informāciju no papildus masīva var aizpildīt sākotnējomasīvu ar jau sakārtotiem elementiem.Algoritma ātrums tiek
        sasniegts sakarā ar to, ka sākotnējā masīva elementi netiek salīdzināti viens ar otru.
    </h3>
</section>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/popper/popper.min.js"></script>
<script src="assets/tether/tether.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/smoothscroll/smooth-scroll.js"></script>
<script src="assets/dropdown/js/script.min.js"></script>
<script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
<script src="assets/theme/js/script.js"></script>
<script src="assets/echarts.common.min.js"></script>

<script type="text/javascript">
    // based on prepared DOM, initialize echarts instance
    var myChart = echarts.init(document.getElementById('firstresult'));

    // specify chart configuration item and data
    var option = {
        tooltip: {},
        legend: {
            data: ['Random no -100 līdz 100', 'secīgi skaitļi no 0 - 10000', 'secīgi skaitļi no 10000 - 0']
        },
        xAxis: {
            data: [
                "{{$firstbound[0]['method']}}",
                "{{$firstbound[1]['method']}}",
                "{{$firstbound[2]['method']}}",
                "{{$firstbound[3]['method']}}",
                "{{$firstbound[4]['method']}}",
                "{{$firstbound[5]['method']}}",
                "{{$firstbound[6]['method']}}",
            ],
            axisLabel: {fontSize: 13},


        },
        yAxis: {
            axisLabel: {fontSize: 16},

        },
        series: [{
            name: 'Random no -100 līdz 100',
            type: 'line',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $firstbound1[0]['times'][0]}},
                {{$firstbound1[1]['times'][0]}},
                {{ $firstbound1[2]['times'][0]}},
                {{$firstbound1[3]['times'][0]}},
                {{$firstbound1[4]['times'][0]}},
                {{$firstbound1[5]['times'][0]}},
                {{$firstbound1[6]['times'][0]}},
            ],

        }, {
            type: 'line',
            name: 'secīgi skaitļi no 0 - 10000',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $firstbound1[0]['times'][1]}},
                {{$firstbound1[1]['times'][1]}},
                {{ $firstbound1[2]['times'][1]}},
                {{$firstbound1[3]['times'][1]}},
                {{$firstbound1[4]['times'][1]}},
                {{$firstbound1[5]['times'][1]}},
                {{$firstbound1[6]['times'][1]}},
            ],

        }, {
            type: 'line',
            name: 'secīgi skaitļi no 10000 - 0',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $firstbound1[0]['times'][2]}},
                {{$firstbound1[1]['times'][2]}},
                {{ $firstbound1[2]['times'][2]}},
                {{$firstbound1[3]['times'][2]}},
                {{$firstbound1[4]['times'][2]}},
                {{$firstbound1[5]['times'][2]}},
                {{$firstbound1[6]['times'][2]}},
            ],

        }],
        dataZoom: [
            {
                type: 'inside',
                start: 0,
                end: 100,

            }, {
                type: 'slider',
                start: 0,
                end: 100,
            }
        ]
    };
    console.log(option);
    // use configuration item and data specified to show chart
    myChart.setOption(option);
    var myChart1 = echarts.init(document.getElementById('secondresult'));

    // specify chart configuration item and data
    var option1 = {
        tooltip: {},
        legend: {
            data: ['Random no -100 līdz 100', 'secīgi skaitļi no 0 - 10000', 'secīgi skaitļi no 10000 - 0']
        },
        xAxis: {
            data: [
                "{{$secondbound2[0]['method']}}",
                "{{$secondbound2[1]['method']}}",
                "{{$secondbound2[2]['method']}}",
                "{{$secondbound2[3]['method']}}",
                "{{$secondbound2[4]['method']}}",
                "{{$secondbound2[5]['method']}}",
                "{{$secondbound2[6]['method']}}",
            ],
            axisLabel: {fontSize: 13},


        },
        yAxis: {
            axisLabel: {fontSize: 16},

        },
        series: [{
            name: 'Random no -100 līdz 100',
            type: 'line',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $secondbound2[0]['times'][0]}},
                {{$secondbound2[1]['times'][0]}},
                {{ $secondbound2[2]['times'][0]}},
                {{$secondbound2[3]['times'][0]}},
                {{$secondbound2[4]['times'][0]}},
                {{$secondbound2[5]['times'][0]}},
                {{$secondbound2[6]['times'][0]}},
            ],

        }, {
            type: 'line',
            name: 'secīgi skaitļi no 0 - 10000',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $secondbound2[0]['times'][1]}},
                {{$secondbound2[1]['times'][1]}},
                {{ $secondbound2[2]['times'][1]}},
                {{$secondbound2[3]['times'][1]}},
                {{$secondbound2[4]['times'][1]}},
                {{$secondbound2[5]['times'][1]}},
                {{$secondbound2[6]['times'][1]}},
            ],

        }, {
            type: 'line',
            name: 'secīgi skaitļi no 10000 - 0',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $secondbound2[0]['times'][2]}},
                {{$secondbound2[1]['times'][2]}},
                {{ $secondbound2[2]['times'][2]}},
                {{$secondbound2[3]['times'][2]}},
                {{$secondbound2[4]['times'][2]}},
                {{$secondbound2[5]['times'][2]}},
                {{$secondbound2[6]['times'][2]}},
            ],

        }],
        dataZoom: [
            {
                type: 'inside',
                start: 0,
                end: 100,

            }, {
                type: 'slider',
                start: 0,
                end: 100,
            }
        ]
    };
    // use configuration item and data specified to show chart
    myChart1.setOption(option1);
    var myChart2 = echarts.init(document.getElementById('thirdresult'));

    // specify chart configuration item and data
    var option2 = {
        tooltip: {},
        legend: {
            data: ['Random no -100 līdz 100', 'secīgi skaitļi no 0 - 10000', 'secīgi skaitļi no 10000 - 0']
        },
        xAxis: {
            data: [
                "{{$thirdbound3[0]['method']}}",
                "{{$thirdbound3[1]['method']}}",
                "{{$thirdbound3[2]['method']}}",
                "{{$thirdbound3[3]['method']}}",
                "{{$thirdbound3[4]['method']}}",
                "{{$thirdbound3[5]['method']}}",
                "{{$thirdbound3[6]['method']}}",
            ],
            axisLabel: {fontSize: 13},


        },
        yAxis: {
            axisLabel: {fontSize: 16},

        },
        series: [{
            name: 'Random no -100 līdz 100',
            type: 'line',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $thirdbound3[0]['times'][0]}},
                {{$thirdbound3[1]['times'][0]}},
                {{ $thirdbound3[2]['times'][0]}},
                {{$thirdbound3[3]['times'][0]}},
                {{$thirdbound3[4]['times'][0]}},
                {{$thirdbound3[5]['times'][0]}},
                {{$thirdbound3[6]['times'][0]}},
            ],

        }, {
            type: 'line',
            name: 'secīgi skaitļi no 0 - 10000',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $thirdbound3[0]['times'][1]}},
                {{$thirdbound3[1]['times'][1]}},
                {{ $thirdbound3[2]['times'][1]}},
                {{$thirdbound3[3]['times'][1]}},
                {{$thirdbound3[4]['times'][1]}},
                {{$thirdbound3[5]['times'][1]}},
                {{$thirdbound3[6]['times'][1]}},
            ],

        }, {
            type: 'line',
            name: 'secīgi skaitļi no 10000 - 0',
            symbolSize: 15,
            lineStyle: {width: 6},
            data: [
                {{ $thirdbound3[0]['times'][2]}},
                {{$thirdbound3[1]['times'][2]}},
                {{ $thirdbound3[2]['times'][2]}},
                {{$thirdbound3[3]['times'][2]}},
                {{$thirdbound3[4]['times'][2]}},
                {{$thirdbound3[5]['times'][2]}},
                {{$thirdbound3[6]['times'][2]}},
            ],

        }],
        dataZoom: [
            {
                type: 'inside',
                start: 0,
                end: 100,

            }, {
                type: 'slider',
                start: 0,
                end: 100,
            }
        ]
    };
    myChart2.setOption(option2);
</script>
</body>
</html>