<?php

namespace App\Http\Controllers;

class Methods extends Controller
{
    public $count = 0;

    public static function shaker($array)
    {
        $l = 0;
        $r = count($array) - 1;

        while ($l < $r) {
            for ($m = $l; $m < $r; $m++) {
                if ($array[$m] > $array[$m + 1]) {
                    $temp = $array[$m];
                    $array[$m] = $array[$m + 1];
                    $array[$m + 1] = $temp;
                }
            }
            $r--;
            for ($m = $r; $m > $l; $m--) {
                if ($array[$m - 1] > $array[$m]) {
                    $temp = $array[$m - 1];
                    $array[$m - 1] = $array[$m];
                    $array[$m] = $temp;
                }
            }
            $l++;

        }
    }

    public static function HorraIrritation($array)
    {
        $left_side[] = count($array) / 2;
        $right_side[] = count($array) / 2;

        $position = 0;


        $left_side[0] = 0;
        $right_side[0] = count($array) - 1;
        $position += 1;
        $l = 0;
        $r = 0;
        while ($position > 0) {
            $position -= 1;
            $l = $left_side[$position];
            $r = $right_side[$position];

            while ($l < $r) {
                $m = $array[$l + ($r - $l) / 2];

                $i = $l;
                $j = $r;

                for (; $i <= $j;) {

                    for (; $array[$i] < $m; $i++) {

                    }
                    for (; $array[$j] > $m; $j--) {

                    }

                    if ($i <= $j) {
                        $temp = $array[$j];
                        $array[$j] = $array[$i];
                        $array[$i] = $temp;
                        $i++;
                        $j--;
                    }
                }

                if ($i < $r) {
                    $left_side[$position] = $i;
                    $right_side[$position] = $r;
                    $position++;
                }

                $r = $j;
            }
        }
        // sakārtot masīvu a izmantojot 2. metodi
    }

    public static function sqareroot($a)
    {
        // 1. metode (Kvadrātsaknes izvēles metode)
        $gCnt = (int)ceil(sqrt(count($a)));
        $gLen = (int)sqrt(count($a));

        $gBorders[0][0] = 0;
        $gBorders[0][1] = $gLen - 1;

        for ($i = 1; $i < $gCnt; $i++) {
            $gBorders[$i][0] = $gBorders[$i - 1][0] + $gLen;
            $gBorders[$i][1] = $gBorders[$i - 1][1] + $gLen;
        }

        $gBorders[$gCnt - 1][1] = count($a) - 1;

        for ($i = 0; $i < $gCnt; $i++) {
            $min = $gBorders[$i][0];

            for ($j = $gBorders[$i][0] + 1; $j <= $gBorders[$i][1]; $j++) {
                if ($a[$min] > $a[$j]) {
                    $min = $j;
                }
            }

            $buf[$i] = $a[$min];
            $a[$min] = 2147483647;
        }


        for ($k = 0; $k < count($a); $k++) {

            $bmin = 0;
            for ($i = 1; $i < $gCnt; $i++) {
                if ($buf[$i] < $buf[$bmin]) {
                    $bmin = $i;
                }
            }

            $b[$k] = $buf[$bmin];
            $buf[$bmin] = 2147483647;

            $min = $gBorders[$bmin][0];

            for ($i = $gBorders[$bmin][0] + 1; $i <= $gBorders[$bmin][1]; $i++) {
                if ($a[$min] > $a[$i]) {
                    $min = $i;
                }
            }

            $buf[$bmin] = $a[$min];
            $a[$min] = 2147483647;

        }

        for ($i = 0; $i < count($a); $i++) {
            $a[$i] = $b[$i];
        }
    }

    public static function Shell($a)
    {  //BubbleSort
        $count = 0;
        for ($j = 0; $j < $count / 2; $j++) {
            for ($i = 1; $i < count($a);
                 $i = $i + 2) {
                if ($a[$i - 1] > $a[$i]) {
                    $tmp = $a[$i];
                    $a[$i] = $a[$i - 1];
                    $a[$i - 1] = $tmp;
                }
            }
            for ($i = 2; $i < count($a); $i = $i + 2) {
                if ($a[$i - 1] > $a[$i]) {
                    $tmp = $a[$i];
                    $a[$i] = $a[$i - 1];
                    $a[$i - 1] = $tmp;
                }
            }
        }
    }

    public static function UpdatedBublesort($a)
    {
        $d = count($a) / 2;
        while ($d > 0) {
            for ($i = 0; $i < (count($a) - $d); $i++) { // until i smaller than half of array
                $j = $i;
                while (($j >= 0) && ($a[$j] > $a[$j + $d])) {
                    $tmp = $a[$j];
                    $a[$j] = $a[$j + $d];
                    $a[$j + $d] = $tmp;
                    $j--;
                }
            }
            $d = $d / 2;
        }
    }

    public static function SecigaApvienosana($a)
    {
        $less =  array_fill(0, count($a), 0);
        $equal =  array_fill(0, count($a), 0);

        for ($i = 0; $i < count($a); $i++) {
            for ($j = 0; $j < count($a); $j++) {
                if ($a[$i] == $a[$j]) {
                    $equal[$i] = $equal[$i] + 1;
                } else if ($a[$i] > $a[$j]) {
                    $less[$i] = $less[$i] + 1;
                }
            }
        }
        for ($i = 0; $i < count($a); $i++) {
            $k = $less[$i];
            for ($j = 0; $j < $equal[$i]; $j++) {
                $b[$k + $j] = $a[$i];
            }
        }
        for ($i = 0; $i < count($a); $i++) {
            $a[$i] = $b[$i];
        }
    }

    public static function KopuSkirosana($a)
    {
        $b = [];
        $c = [];
        $temp = [];
        $len = 1;
        for ($i = 0; $i < count($a); $i++) {
            $b[$i] = $a[$i];
        }
        while ($len < count($a)) {
            $n = 0;
            for ($k = 0; $k < count($b); $k = $k + 2 * $len) {
                $n = $k;
                $i = $k;
                $j = $k + $len;
                if ($k + $len < count($a)) {
                    $ri = $k + $len;
                } else {
                    $ri = count($a);
                }
                if ($k + 2 * $len < count($a)) {
                    $rj = $k + 2 * $len;
                } else {
                    $rj = count($a);
                }
                while ($i < $ri && $j < $rj) {
                    if ($b[$i] < $b[$j]) {
                        $c[$n] = $b[$i];
                        $i = $i + 1;
                        $n = $n + 1;
                    } else {
                        $c[$n] = $b[$j];
                        $j = $j + 1;
                        $n = $n + 1;
                    }
                } // while
                while ($i < $ri) {
                    $c[$n] = $b[$i];
                    $i = $i + 1;
                    $n = $n + 1;
                }
                while ($j < $rj) {
                    $c[$n] = $b[$j];
                    $j = $j + 1;
                    $n = $n + 1;
                }
            } // for
            $len = $len * 2;
            $tmp = $b;
            $b = $c;
            $c = $tmp;
        }
        for ($i = 0; $i < count($a); $i++) {
            $a[$i] = $b[$i];
        }

    }


}