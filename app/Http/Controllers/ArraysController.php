<?php

namespace App\Http\Controllers;

use App\Arrays;
use App\JavaTimes;
use Request;

class ArraysController extends Controller
{
    public $rnd;

    public function index()
    {

        $this->Handler(1, request('arraystep'), request('arraysize'));
        return redirect()->back();

    }

    public function Handler($method_nr, $step, $size)
    {

        for ($q = 1; $q < 4; $q++) {
            for ($i = $step; $i <= $size;) {
                $this->Action($method_nr, $i, $q);
                $i += $step;
            }
            if ($q == 3) {
                $method_nr += 1;
                $this->Handler($method_nr, $step, $size);
            }
        }
    }


    public function Action($method_nr, $arraySize, $type)
    {
        $array = $this->arrayGenerator($arraySize, $type);

        switch ($method_nr) {
            case 1:
                $startTime_1 = microtime(true);
                Methods::shaker($array);
                $endTime_1 = microtime(true);
                $time = $endTime_1 - $startTime_1;
                $this->saveResult($time, $arraySize, $type, $method_nr);
                break;
            case 2:
                $startTime_1 = microtime(true);
                Methods::HorraIrritation($array);
                $endTime_1 = microtime(true);
                $time = $endTime_1 - $startTime_1;
                $this->saveResult($time, $arraySize, $type, $method_nr);
                break;
            case 3:
                $startTime_1 = microtime(true);
                Methods::sqareroot($array);
                $endTime_1 = microtime(true);
                $time = $endTime_1 - $startTime_1;
                $this->saveResult($time, $arraySize, $type, $method_nr);
                break;
            case 4:
                $startTime_1 = microtime(true);
                Methods::Shell($array);
                $endTime_1 = microtime(true);
                $time = $endTime_1 - $startTime_1;
                $this->saveResult($time, $arraySize, $type, $method_nr);
                break;
            case 5:
                $startTime_1 = microtime(true);
                Methods::UpdatedBublesort($array);
                $endTime_1 = microtime(true);
                $time = $endTime_1 - $startTime_1;
                $this->saveResult($time, $arraySize, $type, $method_nr);
                break;
            case 6:
                $startTime_1 = microtime(true);
                Methods::SecigaApvienosana($array);
                $endTime_1 = microtime(true);
                $time = $endTime_1 - $startTime_1;
                $this->saveResult($time, $arraySize, $type, $method_nr);
                break;
            case 7:
                $startTime_1 = microtime(true);
                Methods::KopuSkirosana($array);
                $endTime_1 = microtime(true);
                $time = $endTime_1 - $startTime_1;
                $this->saveResult($time, $arraySize, $type, $method_nr);
                break;
        }
    }

    public function saveResult($time, $arraysize, $type, $method_nr)
    {
        $test = Arrays::where('arraysize', '=', $arraysize)->where('type', '=', $type)->where('method', '=', $method_nr)->get();
        if (count($test) > 0) {

        } else {
            $save = new Arrays();
            $save->method = $method_nr;
            $save->type = $type;
            $save->arraysize = $arraysize;
            $save->time = $time;
            $save->save();
        }

    }

    public function arrayGenerator($arraySize, $type)
    {
        $Masivs = [];
        switch ($type) {
            case 1:
                for ($i = 0; $i < $arraySize; $i++) {
                    $Masivs[$i] = rand(-100, 100);
                }
                break;
            case 2:
                for ($i = 0; $i < $arraySize; $i++) {
                    $Masivs[$i] = $i;
                }
                break;
            case 3:
                for ($i = $arraySize - 1; $i >= 0; $i--) {
                    $Masivs[$i] = $i;
                }
                break;
        }
        $this->rnd = true;
        return $Masivs;
    }

    public function Masivi($size, $startsize, $class)
    {

        // Array methode 1
       $arrtype1Method1 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 1)->where('type', 1)->get());
        $arrtype2Method1 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 1)->where('type', 2)->get());
        $arrtype3Method1 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 1)->where('type', 3)->get());
//end
        $arrtype1Method2 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 2)->where('type', 1)->get());
        $arrtype2Method2 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 2)->where('type', 2)->get());
        $arrtype3Method2 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 2)->where('type', 3)->get());

        $arrtype1Method3 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 3)->where('type', 1)->get());
        $arrtype2Method3 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 3)->where('type', 2)->get());
        $arrtype3Method3 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 3)->where('type', 3)->get());

        $arrtype1Method4 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 4)->where('type', 1)->get());
        $arrtype2Method4 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 4)->where('type', 2)->get());
        $arrtype3Method4 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 4)->where('type', 3)->get());

        $arrtype1Method5 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 5)->where('type', 1)->get());
        $arrtype2Method5 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 5)->where('type', 2)->get());
        $arrtype3Method5 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 5)->where('type', 3)->get());


        $arrtype1Method6 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 6)->where('type', 1)->get());
        $arrtype2Method6 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 6)->where('type', 2)->get());
        $arrtype3Method6 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 6)->where('type', 3)->get());

        $arrtype1Method7 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 7)->where('type', 1)->get());
        $arrtype2Method7 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 7)->where('type', 2)->get());
        $arrtype3Method7 = $this->calculus($class::where('arraysize', '<=', $size)->where('arraysize', '>=', $startsize)->where('method', 7)->where('type', 3)->get());

        $res = [
          [
                'times' => [$arrtype1Method1, $arrtype2Method1, $arrtype3Method1],
                'method' => 'Uzlabota apmaiņas metode (šeikeris) ',
                'size' => $size
            ],

             [
                 'times' => [$arrtype1Method2, $arrtype2Method2, $arrtype3Method2],
                 'method' => ' Horra(QuickSort)',
                 'size' => $size
             ],
            [
                'times' => [$arrtype1Method3, $arrtype2Method3, $arrtype3Method3],
                'method' => 'Kvadrātsaknes',
                'size' => $size
            ],

            [
                'times' => [$arrtype1Method4, $arrtype2Method4, $arrtype3Method4],
                'method' => 'Šella metode ',
                'size' => $size

            ],
            [
                'times' => [$arrtype1Method5, $arrtype2Method5, $arrtype3Method5],
                'method' => 'BubleSort(pāra un nepāra elementu apmaiņas metode)',
                'size' => $size
            ],
             [
                 'times' => [$arrtype1Method6, $arrtype2Method6, $arrtype3Method6],
                 'method' => 'Secīgas apvienošanas kopā šķirošana',
                 'size' => $size
             ],
             [
                 'times' => [$arrtype1Method7, $arrtype2Method7, $arrtype3Method7],
                 'method' => 'Skirosana ar saskaitisanu ',
                 'size' => $size
             ]
        ];
      //  dd($res);
        return $res;

    }


    public function result()
    {
        $class1 = 'App\Arrays';
        $firstbound = $this->Masivi(10000, 10000,$class1);
        $secondbound = $this->Masivi(10000, 100,$class1);
        $thirdbound = $this->Masivi(100, 10,$class1);

        $class1 = 'App\JavaTimes';
        $firstbound1 = $this->Masivi(10000, 10000,$class1);
        $secondbound2 = $this->Masivi(10000, 100,$class1);
        $thirdbound3 = $this->Masivi(100, 10,$class1);
        return view('arrays', compact('firstbound', 'secondbound', 'thirdbound','firstbound1', 'secondbound2', 'thirdbound3'));
    }

    public function calculus($arrSorted1)
    {

        $countByType = count($arrSorted1);

        $time1 = 0;
        foreach ($arrSorted1 as $time) {
            $time1 += $time->time;
        }

        $type1Final = round($time1 / $countByType, 9);
        if($type1Final > 100){
            $type1Final =  number_format($type1Final, 0,'','') ;
    }else{
            $type1Final =  number_format($type1Final , 8);
        }
        return   $type1Final;
    }
}
