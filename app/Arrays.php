<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arrays extends Model
{
    protected $fillable = ['time', 'arraysize', 'type', 'method'];
}
